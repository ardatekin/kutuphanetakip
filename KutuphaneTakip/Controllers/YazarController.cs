﻿using KutuphaneTakip.Models;
using KutuphaneTakip.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data;

namespace KutuphaneTakip.Controllers
{
    [Route("api/yazars")]
    [ApiController]
    public class YazarController : ControllerBase
    {
        private readonly RepositoryContext _context;
        public YazarController(RepositoryContext context)
        {
            _context = context;
        }
        [HttpGet]
        public IActionResult GetAllYazars()
        {
            try
            {
                var yazars = _context.Yazars.ToList();
                return Ok(yazars);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }
        [HttpPost]
        public IActionResult CreateOneYazar([FromBody] Yazar yazar)
        {
            try
            {
                if (yazar is null)
                    return NotFound();

               

                _context.Yazars.Add(yazar);
                _context.SaveChanges();
                return StatusCode(201, yazar);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


    }
}

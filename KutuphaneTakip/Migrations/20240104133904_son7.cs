﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace KutuphaneTakip.Migrations
{
    /// <inheritdoc />
    public partial class son7 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Departmanlars_Kutuphanes_KutuphaneId",
                table: "Departmanlars");

            migrationBuilder.DropForeignKey(
                name: "FK_Kitaplars_Kutuphanes_KutuphaneId",
                table: "Kitaplars");

            migrationBuilder.DropForeignKey(
                name: "FK_Odalars_Kutuphanes_KutuphaneId",
                table: "Odalars");

            migrationBuilder.DropForeignKey(
                name: "FK_OduncVerilenAyars_Kutuphanes_KutuphaneId",
                table: "OduncVerilenAyars");

            migrationBuilder.DropForeignKey(
                name: "FK_Rezervasyons_Kutuphanes_KutuphaneId",
                table: "Rezervasyons");

            migrationBuilder.DropTable(
                name: "Kutuphanes");

            migrationBuilder.CreateTable(
                name: "Kutuphanelers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    KutuphaneAdi = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Kutuphanelers", x => x.Id);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Departmanlars_Kutuphanelers_KutuphaneId",
                table: "Departmanlars",
                column: "KutuphaneId",
                principalTable: "Kutuphanelers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Kitaplars_Kutuphanelers_KutuphaneId",
                table: "Kitaplars",
                column: "KutuphaneId",
                principalTable: "Kutuphanelers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Odalars_Kutuphanelers_KutuphaneId",
                table: "Odalars",
                column: "KutuphaneId",
                principalTable: "Kutuphanelers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_OduncVerilenAyars_Kutuphanelers_KutuphaneId",
                table: "OduncVerilenAyars",
                column: "KutuphaneId",
                principalTable: "Kutuphanelers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Rezervasyons_Kutuphanelers_KutuphaneId",
                table: "Rezervasyons",
                column: "KutuphaneId",
                principalTable: "Kutuphanelers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Departmanlars_Kutuphanelers_KutuphaneId",
                table: "Departmanlars");

            migrationBuilder.DropForeignKey(
                name: "FK_Kitaplars_Kutuphanelers_KutuphaneId",
                table: "Kitaplars");

            migrationBuilder.DropForeignKey(
                name: "FK_Odalars_Kutuphanelers_KutuphaneId",
                table: "Odalars");

            migrationBuilder.DropForeignKey(
                name: "FK_OduncVerilenAyars_Kutuphanelers_KutuphaneId",
                table: "OduncVerilenAyars");

            migrationBuilder.DropForeignKey(
                name: "FK_Rezervasyons_Kutuphanelers_KutuphaneId",
                table: "Rezervasyons");

            migrationBuilder.DropTable(
                name: "Kutuphanelers");

            migrationBuilder.CreateTable(
                name: "Kutuphanes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    KutuphaneAdi = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Kutuphanes", x => x.Id);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Departmanlars_Kutuphanes_KutuphaneId",
                table: "Departmanlars",
                column: "KutuphaneId",
                principalTable: "Kutuphanes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Kitaplars_Kutuphanes_KutuphaneId",
                table: "Kitaplars",
                column: "KutuphaneId",
                principalTable: "Kutuphanes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Odalars_Kutuphanes_KutuphaneId",
                table: "Odalars",
                column: "KutuphaneId",
                principalTable: "Kutuphanes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_OduncVerilenAyars_Kutuphanes_KutuphaneId",
                table: "OduncVerilenAyars",
                column: "KutuphaneId",
                principalTable: "Kutuphanes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Rezervasyons_Kutuphanes_KutuphaneId",
                table: "Rezervasyons",
                column: "KutuphaneId",
                principalTable: "Kutuphanes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

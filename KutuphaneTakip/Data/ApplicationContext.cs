﻿using KutuphaneTakip.Models;

namespace KutuphaneTakip.Data
{
    public static class ApplicationContext
    {


        public static List<Adres> Adress { get; set; }
        public static List<Departmanlar> Departmanlars { get; set; }
        public static List<DilTurleri> DilTurleris { get; set; }
        public static List<Erisebilirlik> Erisebilirliks { get; set; }
        public static List<GeriBildirim> GeriBildirims { get; set; }
        public static List<Kitaplar> Kitaplars { get; set; }
        public static List<KitapTurleri> KitapTurleris { get; set; }
        public static List<Kullanici> Kullanicis { get; set; }
        public static List<KullaniciTipi> KullaniciTipis { get; set; }
        public static List<Kutuphane> Kutuphanes { get; set; }
        public static List<Odalar> Odalars{ get; set; }
        public static List<OdaTipi> OdaTipis{ get; set; }
        public static List<OduncDurumlari> OduncDurumlaris{ get; set; }
        public static List<OduncVerilenAyar> OduncVerilenAyars{ get; set; }
        public static List<OduncVerilenKitaplar> OduncVerilenKitaplars{ get; set; }
        public static List<Rezervasyon> Rezervasyons{ get; set; }
        public static List<Yayinevi> Yayinevis{ get; set; }
        public static List<Yazar> Yazars{ get; set; }
    }
}
